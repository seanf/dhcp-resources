# Extended dHCP Augmented Volumetric Atlas 


[[_TOC_]]

The `extended` dHCP volumetric atlas (not yet released) extends the `original` [dHCP volumetric atlas](https://gin.g-node.org/BioMedIA/dhcp-volumetric-atlas-groupwise) to span 28-44 weeks PMA.  It is a complete reconstruction of that atlas, therefore the week templates are *NOT* aligned between the `original` and `extended` atlas.  However a transform is provided between the 40-week templates of the `original` and `extended` atlas.

>T2w (upper row) and T1w (lower row) for weeks 28-44 PMA of the `extended` dHCP volumetric atlas.

![alt text](templates.jpg)

The `extended` atlas has been **augmented** here to have filenames and folder structure compatible with the dHCP neonatal fMRI pipeline, and to contain week-to-week transforms and inter-atlas transforms.

## Download (~4.2 GB)

https://users.fmrib.ox.ac.uk/~seanf/dhcp-augmented-volumetric-atlas-extended.tar.gz

## Atlas Citation
Andreas Schuh, Antonios Makropoulos, Emma C. Robinson, Lucilio Cordero-Grande, Emer Hughes, Jana Hutter, Anthony N. Price, Maria Murgasova, Rui Pedro A. G. Teixeira, Nora Tusor, Johannes Steinweg, Suresh Victor, Mary A. Rutherford, Joseph V. Hajnal, A. David Edwards, and Daniel Rueckert, **Unbiased construction of a temporally consistent morphological atlas of neonatal brain development**, *bioRxiv*, 2018. **doi:** https://doi.org/10.1002/mrm.26796

```
@article {Schuh2018,
  author = {Schuh, Andreas and Makropoulos, Antonios and Robinson, Emma C. and Cordero-Grande, Lucilio and Hughes, Emer and Hutter, Jana and Price, Anthony N and Murgasova, Maria and Teixeira, Rui Pedro A. G. and Tusor, Nora and Steinweg, Johannes K. and Victor, Suresh and Rutherford, Mary A. and Hajnal, Joseph V. and Edwards, A. David and Rueckert, Daniel},
  title = {Unbiased construction of a temporally consistent morphological atlas of neonatal brain development},
  journal = {bioRxiv},
  year = {2018},
  doi = {10.1101/251512},
  publisher = {Cold Spring Harbor Laboratory}
}
```

## Atlas Structure

```bash
atlas-extended
├── LICENSE.md
├── README.md
├── atlas.tree
├── inter-atlas
│   ├── MNI152NLin6ASym_to_MNI152NLin2009aAsym_invwarp.nii.gz
│   ├── MNI152NLin6ASym_to_MNI152NLin2009aAsym_warp.nii.gz
│   ├── dhcp40wk_to_MNI152NLin2009aAsym_invwarp.nii.gz
│   ├── dhcp40wk_to_MNI152NLin2009aAsym_warp.nii.gz
│   ├── dhcp40wk_to_MNI152NLin6ASym_invwarp.nii.gz
│   ├── dhcp40wk_to_MNI152NLin6ASym_warp.nii.gz
│   ├── dhcp40wk_to_extdhcp40wk_invwarp.nii.gz
│   ├── dhcp40wk_to_extdhcp40wk_warp.nii.gz
│   ├── extdhcp40wk_to_MNI152NLin2009aAsym_invwarp.nii.gz
│   ├── extdhcp40wk_to_MNI152NLin2009aAsym_warp.nii.gz
│   ├── extdhcp40wk_to_MNI152NLin6ASym_invwarp.nii.gz
│   ├── extdhcp40wk_to_MNI152NLin6ASym_warp.nii.gz
│   ├── extdhcp44wk_to_MNI152NLin2009aAsym_invwarp.nii.gz
│   └── extdhcp44wk_to_MNI152NLin2009aAsym_warp.nii.gz
├── template
│   ├── week28_T1w.nii.gz
│   ├── week28_T2w.nii.gz
│   ├── week28_tissue_dseg.nii.gz
│   ├── week28_tissue_probseg.nii.gz
│   ├── ...
│   ├── week44_T1w.nii.gz
│   ├── week44_T2w.nii.gz
│   ├── week44_tissue_dseg.nii.gz
│   └── week44_tissue_probseg.nii.gz
└── warps
    ├── week-28_to_week-40_invwarp.nii.gz
    ├── week-28_to_week-40_warp.nii.gz
    ├── ...
    ├── week-44_to_week-40_invwarp.nii.gz
    └── week-44_to_week-40_warp.nii.gz

```

## Augmented Registration Details

The `extended` dHCP atlas has been augmented with week-to-week transforms, and inter-atlas transforms.

### Space labels

| Label | Description |
| --- | --- |
| `dhcp40wk` | dHCP atlas week-40 volumetric template space |
| `extdhcp40wk` | dHCP **extended** atlas week-40 volumetric template space |
| `MNI152NLin2009aAsym` | MNI **2009** asymmetric atlas |
| `MNI152NLin6ASym` | MNI **6th gen** asymmetric atlas (is in FSL) |

### Transform naming

The  transform filenames contain an `origin` value and a `ref` value to indicate the direction that the registration was calculated (or combined if a composite).  The `origin` value is the space-label for the *origin/source/moving* image, and the `ref` value is the space-label for the *reference/target* image. These transforms have the suffix `warp` to indicate that it is forward and nonlinear, and the suffix `invwarp` to indicate that it is the inverse of the forward warp/composite.

```
{origin}_to_{ref}_warp.nii.gz
{origin}_to_{ref}_invwarp.nii.gz
```

### Week-to-week
1. Week-to-week registration was performed with a non-linear registration ([ANTs SyN](http://stnava.github.io/ANTs/)) of the `week-{t}` T2w template to the `week-{t+1}` T2w template.
2. The inverse for each of transform was calculated.
3. The `week-{t}` to `week-{t+1}` transforms were combined sequentially to yield a single warp/transform from each week template to every other week template.

> **Upper:** T2w week templates (28-44) resampled to the 40-week template (base image) with the WM (green overlay) and GM (red overlay) 40-week tissue segmentations, of the `extended` dHCP volumetric atlas. **Lower:** 40-week template resampled to the respective week space (base image) with the WM (green overlay) and GM (red overlay) respective-week tissue segmentations, of the `extended` dHCP volumetric atlas.

![alt text](weekly-warps.jpg)

### Original-to-extended
The week-40 T2w template from the `original` atlas (`dhcp40wk`) was non-linearly registered ([ANTs SyN](http://stnava.github.io/ANTs/)) to the week-40 T2w template from the `extended` atlas (`extdhcp40wk`).

```bash
dhcp40wk_to_extdhcp40wk_invwarp.nii.gz
dhcp40wk_to_extdhcp40wk_warp.nii.gz
```

> **Upper:** 40-week T2w template from the `original` atlas (`dhcp40wk`) resampled to the 40-week template of the `extended` atlas (`extdhcp40wk`; base image) with the WM (green overlay) and GM (red overlay) 40-week `extended` atlas tissue segmentations. **Lower:** 40-week T2w template from the `extended` atlas  resampled to the 40-week template of the `original` atlas (base image) with the WM (green overlay) and GM (red overlay) 40-week `original` atlas tissue segmentations.

![alt text](original-to-extended.jpg)

### MNI(6th gen)-to-MNI(2009)

The [MNI152 6th gen](http://nist.mni.mcgill.ca/?p=858) atlas (`MNI152NLin6ASym`) was aligned to the [MNI152 2009](http://nist.mni.mcgill.ca/?p=904) atlas (`MNI152NLin2009aAsym`) with a nonlinear registration ([ANTs SyN](http://stnava.github.io/ANTs/)) using the T1w modality.  

```bash
MNI152NLin6ASym_to_MNI152NLin2009aAsym_invwarp.nii.gz
MNI152NLin6ASym_to_MNI152NLin2009aAsym_warp.nii.gz
```

> **Upper:** MNI152 6th gen asymmetric atlas T1w (`MNI152NLin6ASym`) resampled to the MNI152 2009 asymmetric atlas (`MNI152NLin2009aAsym`) space (base image), with the WM (green overlay) and GM (red overlay) MNI152 2009 atlas tissue segmentations. **Lower:** MNI152 2009 asymmetric atlas T1w resampled to the MNI152 6th gen asymmetric atlas space (base image), with the contrast edges of the MNI152 6th gen T1w (red overlay).

![alt text](mni-to-mni.jpg)

### dhcp (original and extended) to MNI2009
 
The week-44 extended dHCP atlas space (`extdhcp44wk`) was aligned to the [MNI152 2009](http://nist.mni.mcgill.ca/?p=904) atlas (`MNI152NLin2009aAsym`) with a multimodal nonlinear registration ([ANTs SyN](http://stnava.github.io/ANTs/)) using the T2w, and GM and WM probality as modalities.  This warp was then combined with week-to-week and inter-atlas warps to yield composite warps from both the **original** and **extended** 40-week dhcp atlases to the [MNI152 2009](http://nist.mni.mcgill.ca/?p=904) atlas.

```bash
extdhcp44wk_to_MNI152NLin2009aAsym_invwarp.nii.gz
extdhcp44wk_to_MNI152NLin2009aAsym_warp.nii.gz
extdhcp40wk_to_MNI152NLin2009aAsym_invwarp.nii.gz
extdhcp40wk_to_MNI152NLin2009aAsym_warp.nii.gz
dhcp40wk_to_MNI152NLin2009aAsym_invwarp.nii.gz
dhcp40wk_to_MNI152NLin2009aAsym_warp.nii.gz
```

> dHCP T2w `original` and `extended` templates resampled to the MNI152 2009 space, and the inverse.  Overlays are the WM (green) and GM (red) tissue segmentations of the respective resampled atlas space.

![alt text](dhcp-to-mni2009.jpg)

### dhcp (original and extended) to MNI 6th gen

The previously described week-to-week and inter-atlas warps were combined to yield composite warps from both the **original** and **extended** 40-week dhcp atlases to the [MNI152 6th gen](http://nist.mni.mcgill.ca/?p=858) atlas.

```bash
dhcp40wk_to_MNI152NLin6ASym_invwarp.nii.gz
dhcp40wk_to_MNI152NLin6ASym_warp.nii.gz
extdhcp40wk_to_MNI152NLin6ASym_invwarp.nii.gz
extdhcp40wk_to_MNI152NLin6ASym_warp.nii.gz
```

> dHCP T2w `original` and `extended` templates resampled to the MNI152 6th gen space, and the inverse.  Overlays are the WM (green) and GM (red) tissue segmentations in the dHCP space, or the contrast edges of the MNI152 6th gen T1w.

![alt text](dhcp-to-mni6.jpg)

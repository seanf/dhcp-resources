# dHCP Augmented Volumetric Atlas

[[_TOC_]]

The `original` [dHCP volumetric atlas](https://gin.g-node.org/BioMedIA/dhcp-volumetric-atlas-groupwise) is a spatio-temporal neonatal brain atlas constructed as part of the [Developing Human Connectome Project (dHCP)](http://www.developingconnectome.org/project/), comprised of T2w and T1w templates from 36-44 weeks post-menstrual age (PMA).  The atlas is distributed under the terms of the [Creative Commons Attribution 4.0 International](https://gin.g-node.org/BioMedIA/dhcp-volumetric-atlas-groupwise/src/master/LICENSE.md) license.  

The atlas has been **augmented** here to have filenames and folder structure compatible with the [dHCP neonatal fMRI pipeline](https://git.fmrib.ox.ac.uk/seanf/dhcp-neonatal-fmri-pipeline), and to contain week-to-week transforms.

## Download (~7.3 GB)

https://users.fmrib.ox.ac.uk/~seanf/dhcp-augmented-volumetric-atlas.tar.gz

## Atlas Citation
Andreas Schuh, Antonios Makropoulos, Emma C. Robinson, Lucilio Cordero-Grande, Emer Hughes, Jana Hutter, Anthony N. Price, Maria Murgasova, Rui Pedro A. G. Teixeira, Nora Tusor, Johannes Steinweg, Suresh Victor, Mary A. Rutherford, Joseph V. Hajnal, A. David Edwards, and Daniel Rueckert, **Unbiased construction of a temporally consistent morphological atlas of neonatal brain development**, *bioRxiv*, 2018. **doi:** https://doi.org/10.1002/mrm.26796

```
@article {Schuh2018,
  author = {Schuh, Andreas and Makropoulos, Antonios and Robinson, Emma C. and Cordero-Grande, Lucilio and Hughes, Emer and Hutter, Jana and Price, Anthony N and Murgasova, Maria and Teixeira, Rui Pedro A. G. and Tusor, Nora and Steinweg, Johannes K. and Victor, Suresh and Rutherford, Mary A. and Hajnal, Joseph V. and Edwards, A. David and Rueckert, Daniel},
  title = {Unbiased construction of a temporally consistent morphological atlas of neonatal brain development},
  journal = {bioRxiv},
  year = {2018},
  doi = {10.1101/251512},
  publisher = {Cold Spring Harbor Laboratory}
}
```

## Atlas Structure

```bash
atlas
├── T1 # T1 templates per week
│   ├── template-36.nii.gz
│   ├── ...
│   └── template-44.nii.gz
├── T2 # T2 templates per week
│   ├── template-36.nii.gz
│   ├── ...
│   └── template-44.nii.gz
├── allwarps # week-to-week FSL FNIRT warps
│   ├── template-36_to_template-37_warp.nii.gz
│   ├── ...
│   └── template-44_to_template-43_warp.nii.gz
├── atlas.tree
└── labels      
    ├── structures # structure labels per week
    │   ├── label-36.nii.gz
    │   ├── ...
    │   └── label-44.nii.gz
    └── tissues # tissue labels per week
        ├── label-36.nii.gz
        ├── ...
        └── label-44.nii.gz
```

## Augmented Registration Details

The dHCP atlas has been augmented with week-to-week transforms.

### Week-to-week
1. Week-to-week registration was performed with a multi-modal non-linear registration ([ANTs SyN](http://stnava.github.io/ANTs/)) of the `template-{week}` T1w & T2w template to the `template-{week+1}` T1w & T2w template.
2. The inverse for each of transform was calculated.
3. The `template-{week}_to_template-{week+1}` transforms were combined sequentially to yield a single warp/transform from each week template to 40-week template.

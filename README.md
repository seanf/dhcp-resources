## dHCP fMRI Pipeline Resources

This repository contains a number of (large) resources required for the [dHCP neonatal fMRI pipeline](https://git.fmrib.ox.ac.uk/seanf/dhcp-neonatal-fmri-pipeline):

1. [dHCP augmented volumetric atlas (original)](docs/dhcp-augmented-volumetric-atlas.md)
2. [dHCP augmented volumetric atlas (extended)](docs/dhcp-augmented-volumetric-atlas-extended.md)
3. [dHCP trained FIX model](docs/dhcp-trained-fix.md)
4. [dHCP group maps](docs/dhcp-group-maps.md)
5. [dHCP group QC](docs/dhcp-group-qc.md)

## How to download

There is a download link on each of the resource pages listed above.  

Alternatively, if you have installed the [dHCP neonatal fMRI pipeline](https://git.fmrib.ox.ac.uk/seanf/dhcp-neonatal-fmri-pipeline), then you can download the resources automatically with the following command:
```shell
dhcp_fetch_resources.py --path=<path-to-download-to>
```


